# Copyright 2019-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Game Engine"
HOMEPAGE="godotengine.org/"
SRC_URI="https://github.com/godotengine/godot/archive/${PV}-stable.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="+udev +pulseaudio xinerama "

DEPEND="sys-devel/gcc
		dev-util/scons
		dev-lang/yasm"

RDEPEND="${DEPEND}
		xinerama? ( x11-libs/libXinerama )
		pulseaudio? ( media-sound/pulseaudio )
		x11-libs/libX11
		x11-libs/libXcursor
		x11-libs/libXi
		media-libs/mesa
		media-libs/glu
		media-sound/pulseaudio
		media-libs/alsa-lib
		dev-lang/python"

build_opts="tools=yes"

S="${WORKDIR}/${P}-stable"

src_configure() {
	MYSCONS=(
		CC=gcc
		CXX=g++
		module_enet_enabled=yes
		module_freetype_enabled=yes
		module_mbedtls_enabled=yes
		module_opus_enabled=yes
		module_theora_enabled=yes
		module_vorbis_enabled=yes
		module_webp_enabled=yes
		platform=x11
		progress=yes
		pulseaudio=$(usex pulseaudio)
		target=release_debug
		tools=yes
		udev=$(usex udev)
		use_lld=no
		use_llvm=no
		use_lto=yes
		verbose=yes
	)
}

src_compile() {
	scons "${MAKEOPTS}" "${MYSCONS[@]}"
}
src_install() {
	mv ${S}/bin/godot.x11.opt.tools.64 ${S}/bin/godot
	dobin ${S}/bin/godot
}
