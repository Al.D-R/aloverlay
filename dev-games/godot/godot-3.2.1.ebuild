# Copyright 2019-2020 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Game Engine"
HOMEPAGE="godotengine.org/"
SRC_URI="https://github.com/godotengine/godot/archive/${PV}-stable.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE="clang debug udev xinerama "

DEPEND="sys-devel/gcc
		dev-util/scons
		dev-lang/yasm"

RDEPEND="${DEPEND}
		clang? ( sys-devel/clang )
		xinerama? ( x11-libs/libXinerama )
		x11-libs/libX11
		x11-libs/libXcursor
		x11-libs/libXi
		media-libs/mesa
		media-libs/glu
		media-sound/pulseaudio
		media-libs/alsa-lib
		dev-lang/python"

build_opts="tools=yes"

S="${WORKDIR}/${P}-stable"

src_configure() {
	if use clang; then
		${build_opts} = "${build_opts} use_llvm=yes"
	else
		${build_opts} = "${build_opts} use_llvm=no"
	fi
	if use udev; then
		${build_opts} = "${build_opts} udev=yes"
	else
		${build_opts} = "${build_opts} udev=no"
	fi
	if use amd64; then
		${build_opts}="${build_opts} bits=64"
	elif use x86; then
		${build_opts}="${build_opts} bits=32"
	fi
}

src_compile() {
	scons ${MAKEOPTS} platform=x11 target=release_debug ${build_opts}
}
src_install() {
	mv ${S}/bin/godot.x11.opt.tools.64 ${S}/bin/godot
	dobin ${S}/bin/godot
}
