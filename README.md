# **WARNING**
**This 'overlay' is for my personal usage, use it at your own risks**
Some options might not be adequate for your hardware.

# **Installation**
To install this overlay, just copy the following lines in `/etc/portage/repos.conf/yago.conf` : 

```
[yago]

location = /usr/local/portage/yago
sync-type = git
sync-uri = https://gitlab.com/Al.D-R/aloverlay.git
priority = 50
auto-sync = Yes
```
