# Copyright 1999-2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="Non-linear video editor"
HOMEPAGE="https://www.olivevideoeditor.org/"
SRC_URI="https://github.com/olive-editor/olive/archive/0.1.2.tar.gz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64 x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND} media-libs/opencolorio"
BDEPEND=""

S="${WORKDIR}/olive-0.1.2"

src_configure() {
	mkdir build
	cd build
	cmake ..
}

src_compile() {
	cd build
	make ${MAKEOPTS}
}

src_install() {
	cd build
	dobin olive-editor
}
